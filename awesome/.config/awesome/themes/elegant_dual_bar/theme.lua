local elegant_base = require "themes.elegant_base.theme"
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local theme = elegant_base

theme.titlebar      =   false
theme.calendar_loc  =   "tc"
theme.variant       =   "dual-bar"
theme.border_width  =   dpi(2)

return theme;
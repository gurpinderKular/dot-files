#!/bin/bash

sudo pacman -Syyu --noconfirm

echo "Installing packages"
sudo pacman -S stow wget curl htop lxappearance qt5ct kvantum nitrogen lxrandr bluez bluez-utils blueberry feh alacritty playerctl pavucontrol ranger rofi rofi-calc rofi-emoji imagemagick libsecret polkit lxqt-policykit networkmanager-openvpn openvpn libimobiledevice udisks2 ntfs-3g yt-dlp qbittorrent seahorse vim pcmanfm-qt file-roller zathura xournalpp cbatticon vlc mpv gimp flameshot firefox bitwarden krita krita-plugin-gmic gnome-keyring gpick gthumb inkscape docker docker-compose gvfs handbrake shotcut libreoffice-fresh obs-studio ffmpeg obsidian mousepad zsh noto-fonts noto-fonts-emoji ttf-fira-code ttf-fira-sans ttf-ibm-plex ttf-jetbrains-mono fzf --noconfirm

stow */ --adopt

git clone https://github.com/lcpz/lain.git ~/.config/awesome/lain

# install aur helper
yaypath="/home/$USER/yay"
git clone https://aur.archlinux.org/yay.git $yaypath
cd $yaypath
makepkg -si


# enable services
sudo systemctl enable bluetooth

# fix bluetooth icon
gsettings set org.blueberry use-symbolic-icons false

# install aur packages
yes y | yay -S brave-bin downgrade fastfetch insomnia-bin mailspring mongodb-compass zoom timeshift-bin peazip-gtk2-bin ttf-font-awesome-5 volctl xinit-xsession visual-studio-code-bin --answerdiff None --answerclean None

# make zsh default shell
chsh -s /usr/bin/zsh


local elegant_base = require "themes.elegant_base.theme"
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local awesome_theme_path = '~/.config/awesome/themes/elegant_base'

local theme = elegant_base

theme.titlebar      =   true
theme.calendar_loc  =   "br"
theme.variant       =   "single-bar"
theme.de_mode       =  true

theme.useless_gap           =   dpi(2)
theme.border_width          =   dpi(0)

-- tasklist
theme.tasklist_bg_image_normal = awesome_theme_path.."/tasklist/normal.png"
theme.tasklist_bg_image_focus = awesome_theme_path.."/tasklist/active.png"
theme.tasklist_bg_image_urgent = awesome_theme_path.."/tasklist/active.png"
theme.tasklist_bg_image_minimize = awesome_theme_path.."/tasklist/minimized.png"


return theme;
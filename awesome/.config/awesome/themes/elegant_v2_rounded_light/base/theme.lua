local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()
local awesome_theme_path = "~/.config/awesome/themes/elegant_v2_rounded_light/base"

local theme = {}

local font = "monospace"
local font_size = 10
local main_font = font .. " " .. font_size

theme.font = main_font
theme.icon_font = "Font Awesome 6 Free,Font Awesome 6 Brands"

-- COLORS
local primary_bg_color = "#FAFAFA"
local primary_fg_color = "#d8d8d8"

local active_bg_color = "#2f2f2f"
local active_fg_color = "#131313"
local inactive_bg_color = "transparent"
local inactive_fg_color = "#C9D6DF"
local minimize_bg_color = primary_bg_color
local minimize_fg_color = "#44475a"
local urgent_bg_color = "#ff5555"
local urgent_fg_color = "#f8f8f2"
local occupied_bg_color = "transparent"
local occupied_fb_color = "#52616B"
local active_border_color = "#7258ED" -- "#94e2d5" --
local inactive_border_color = "#3F4247"

-- titlebar
theme.titlebar_bg           =   "linear:1000,0,0:55,55,50:0,#C954D2:1,#6D58EF:1,#9E56DF" --"#9156E4"
theme.titlebar_bg_normal    =   "#1A1B26"

-- window
theme.useless_gap = dpi(0)
theme.border_width = dpi(2)
theme.border_normal = inactive_border_color
theme.border_focus = active_border_color

-- bar
theme.bg_normal = primary_bg_color
theme.fg_normal = primary_fg_color
theme.bg_focus = active_bg_color
theme.fg_focus = active_fg_color
theme.bg_unfocused = inactive_bg_color
theme.fg_unfocused = inactive_fb_color
theme.bg_urgent = urgent_bg_color
theme.fg_urgent = urgent_fg_color
theme.bg_minimize = minimize_bg_color
theme.fg_minimize = minimize_fg_color
theme.bg_systray = primary_bg_color

-- widgets color scheme
theme.primary_widget_bg_color = "#D8D8D8"
theme.primary_widget_fg_color = primary_fg_color


--clock
theme.clock_widget_bg_color = "linear:0,0,200:55,55,50:0,#61C0BF:1,#EEF1FF:1,#EEF1FF"
theme.clock_widget_fg_color = "#131313"

-- battery
theme.battery_widget_bg_color = "linear:0,0,20:55,55,50:0,#FFB0CD:1,#EEF1FF:1,#EEF1FF"
theme.battery_widget_fg_color = "#131313"

-- cpu
theme.cpu_widget_bg_color = "linear:0,0,100:0,0,0:0,#A6DCEF:1,#EEF1FF:1,#EEF1FF"
theme.cpu_widget_fg_color = "#131313"

-- fs
theme.fs_widget_bg_color = "linear:0,0,100:35,65,0:0,#B1B2FF:1,#EEF1FF:1,#EEF1FF"
theme.fs_widget_fg_color = "#131313"
theme.fs2_widget_fg_color = "#131313"

-- mem
theme.mem_widget_bg_color = "linear:0,0,100:55,55,0:0,#F6BA6F:1,#EEF1FF:1,#EEF1FF"
theme.mem_widget_fg_color = "#131313"

-- systray
theme.systray_widget_bg_color = "transparent"
theme.systray_widget_fg_color = "#ffffff" -- "#eba0ac"

-- username
theme.username_widget_bg_color = "linear:0,0,100:55,55,50:0,#6D58EF:1,#C954D2:1,#9E56DF"
theme.username_widget_fg_color = "#f2f2f2" -- "#eba0ac"

-- tag list
theme.taglist_bg_focus = "transparent"
theme.taglist_fg_focus = "#131313"
theme.taglist_bg_urgent = urgent_bg_color
theme.taglist_fg_urgent = urgent_fg_color
theme.taglist_bg_occupied = "transparent"
theme.taglist_fg_occupied = "#3E4246"
theme.taglist_bg_empty = theme.primary_widget_bg_color
theme.taglist_bg = theme.primary_widget_bg_color --"#c4a7e7" --"#9ccfd8"
theme.taglist_fg_empty = "#BFC0C0"
theme.taglist_font = theme.icon_font .. " " .. 10
--theme.taglist_spacing           = 5

-- task list
theme.tasklist_bg_focus = theme.primary_widget_bg_color
theme.tasklist_fg_focus = "#131313" -- active_fg_color
theme.tasklist_bg_normal = "transparent"
theme.tasklist_fg_normal = "#3E4246"
theme.tasklist_bg_minimize = "transparent"
theme.tasklist_fg_minimize = "#BFC0C0"
theme.tasklist_icon_size = 64
theme.tasklist_border_color = "#d8d8d8"

-- menu
theme.menu_submenu_icon = nil
theme.menu_height = dpi(30)
theme.menu_width = dpi(250)
theme.menu_bg_focus = "#d8d8d8"
theme.menu_bg_normal = theme.bg_normal
theme.menu_fg_focus = "#131313"
theme.menu_fg_normal = "#393E46"
theme.menu_submenu = "> "
theme.menu_border_color = "#d8d8d8"
theme.menu_border_width = dpi(1)

-- notification
theme.notification_border_width = 2
theme.notification_border_color = "#d8d8d8"
theme.notification_max_width = 350
theme.notification_height = 100
theme.notification_max_height = 150
theme.notification_icon_size = 72
theme.notification_bg = primary_bg_color
theme.notification_fg = "#131313"
theme.notification_margin = dpi(12)
-- theme.notification_opacity = 1

-- calendar popup
cal_bg = "#d8d8d8"

theme.calendar_yearheader_fg_color = "#131313"
theme.calendar_yearheader_bg_color = "transparent"
theme.calendar_header_fg_color = "#131313"
theme.calendar_header_bg_color = cal_bg

theme.calendar_normal_fg_color = "#131313"
theme.calendar_normal_bg_color = cal_bg
theme.calendar_focus_fg_color = "#ffffff"
theme.calendar_focus_bg_color = "#131313"

theme.calendar_year_fg_color = "#131313"
theme.calendar_year_bg_color = "#fafafa"
theme.calendar_year_border_color = "#BFC0C0"
theme.calendar_year_border_width = 1
theme.calendar_year_padding = 10

theme.calendar_month_fg_color = "#131313"
theme.calendar_month_bg_color = cal_bg
theme.calendar_month_padding = 4
theme.calendar_month_border_color = "#BFC0C0"
theme.calendar_month_border_width = 1

theme.calendar_weekday_fg_color = "#131313"
theme.calendar_weekday_bg_color = cal_bg
theme.calendar_weekday_border_width = 0
theme.calendar_weeknumber_border_width = 0
theme.calendar_normal_border_width = 0
theme.calendar_header_border_width = 0
theme.calendar_yearheader_border_width = 0
theme.calendar_weeknumber_border_color = "transparent"


-- system tray
theme.systray_icon_spacing = 6

-- wallpaper
-- theme.wallpaper         =   themes_path.."elegant/background.jpg"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(taglist_square_size, "#131313")
-- theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
--     taglist_square_size, theme.fg_normal
-- )

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
-- theme.menu_submenu_icon = awesome_theme_path.."/submenu.png"
-- theme.menu_height = dpi(15)
-- theme.menu_width  = dpi(96)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = awesome_theme_path .. "/titlebar/close_normal.png"
theme.titlebar_close_button_focus = awesome_theme_path .. "/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = awesome_theme_path .. "/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus = awesome_theme_path .. "/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = awesome_theme_path .. "/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive = awesome_theme_path .. "/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = awesome_theme_path .. "/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active = awesome_theme_path .. "/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = awesome_theme_path .. "/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive = awesome_theme_path .. "/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = awesome_theme_path .. "/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active = awesome_theme_path .. "/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = awesome_theme_path .. "/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive = awesome_theme_path .. "/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = awesome_theme_path .. "/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active = awesome_theme_path .. "/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = awesome_theme_path .. "/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive = awesome_theme_path .. "/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = awesome_theme_path .. "/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active = awesome_theme_path .. "/titlebar/maximized_focus_active.png"

theme.wallpaper = awesome_theme_path .. "/background.jpg"

-- You can use your own layout icons like this:
theme.layout_fairh = awesome_theme_path .. "/layouts/fairhw.png"
theme.layout_fairv = awesome_theme_path .. "/layouts/fairvw.png"
theme.layout_floating = awesome_theme_path .. "/layouts/floatingw.png"
theme.layout_magnifier = awesome_theme_path .. "/layouts/magnifierw.png"
theme.layout_max = awesome_theme_path .. "/layouts/maxw.png"
theme.layout_fullscreen = awesome_theme_path .. "/layouts/fullscreenw.png"
theme.layout_tilebottom = awesome_theme_path .. "/layouts/tilebottomw.png"
theme.layout_tileleft = awesome_theme_path .. "/layouts/tileleftw.png"
theme.layout_tile = awesome_theme_path .. "/layouts/tilew.png"
theme.layout_tiletop = awesome_theme_path .. "/layouts/tiletopw.png"
theme.layout_spiral = awesome_theme_path .. "/layouts/spiralw.png"
theme.layout_dwindle = awesome_theme_path .. "/layouts/dwindlew.png"
theme.layout_cornernw = awesome_theme_path .. "/layouts/cornernww.png"
theme.layout_cornerne = awesome_theme_path .. "/layouts/cornernew.png"
theme.layout_cornersw = awesome_theme_path .. "/layouts/cornersww.png"
theme.layout_cornerse = awesome_theme_path .. "/layouts/cornersew.png"

-- tasklist
-- theme.tasklist_bg_image_normal = awesome_theme_path.."/tasklist/normal.png"
-- theme.tasklist_bg_image_focus = awesome_theme_path.."/tasklist/active.png"
-- theme.tasklist_bg_image_urgent = awesome_theme_path.."/tasklist/active.png"
-- theme.tasklist_bg_image_minimize = awesome_theme_path.."/tasklist/minimized.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(theme.menu_height, theme.bg_focus, theme.fg_focus)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80

#!/bin/bash

BAT="/sys/class/power_supply"

if [ -d $BAT"/BAT0" ] || [ -d $BAT"/BAT1" ]; then
	bat_info=$(acpi -b)
	bat_powerstatus=$(echo $bat_info | awk '{print $3}' | sed 's/[^a-z  A-Z]//g')
	bat_percentage=$(echo $bat_info | awk '{print $4}' | sed 's/%,//g')

	case $bat_powerstatus in
		Charging)
			echo $bat_percentage"% [C] "
			;;
		Discharging)
			if [ $bat_percentage -le 20 ]; then
				notify-send "Warning" "Battery is running low" -u critical -t 15000
				echo $bat_percentage"% "
			fi
			echo $bat_percentage"% "
			;;
		*)
			echo $bat_percentage" "
			;;
	esac
else
	echo "NA "
fi


local elegant_base = require "themes.elegant_v2_rounded.base.theme"
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local theme = elegant_base

theme.titlebar      =   true
theme.calendar_loc  =   "tr"
theme.variant       =   "single-bar"
theme.bar_location  =   "top"
theme.rounded_widget = true

theme.useless_gap           =   dpi(4)
theme.border_width          =   dpi(0)

return theme;
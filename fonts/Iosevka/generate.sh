#!/bin/bash

# Important: By default, the build system will schedule a number of concurrently running jobs equal to the number of threads available on the CPU, which will push CPU usage and also likely RAM usage, if you do not have very much to work with, to the ceiling (each job consumes more than 1 GB of RAM at its peak). If this is an issue for you, pass an additional argument --jCmd=<number of concurrent jobs>.

jobs=3

npm run build -- ttf::iosevka-Menlo --jCmd=$jobs
npm run build -- ttf::iosevka-FiraMono --jCmd=$jobs
npm run build -- ttf::iosevka-Pragmata --jCmd=$jobs
npm run build -- ttf::iosevka-SourceCodePro --jCmd=$jobs
npm run build -- ttf::iosevka-EnvyCodeR --jCmd=$jobs
npm run build -- ttf::iosevka-UbuntuMono --jCmd=$jobs
npm run build -- ttf::iosevka-Lucida --jCmd=$jobs
npm run build -- ttf::iosevka-JetBrainsMono --jCmd=$jobs
npm run build -- ttf::iosevka-IBMPlex --jCmd=$jobs
npm run build -- ttf::iosevka-PTMono --jCmd=$jobs
npm run build -- ttf::iosevka-RecursiveMono --jCmd=$jobs
npm run build -- ttf::iosevka-InputMono --jCmd=$jobs
npm run build -- ttf::iosevka-Curly --jCmd=$jobs

#!/bin/bash

chmod -R 755 xinitrcsession-helper
chmod -R 644 xinitrc.desktop

cp xinitrcsession-helper /usr/bin/xinitrcsession-helper
cp xinitrc.desktop /usr/share/xsessions/xinitrc.desktop


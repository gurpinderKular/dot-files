pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")
local HOME_PATH = os.getenv("HOME")
local USERNAME = os.getenv("USER")
local active_screen = awful.screen.preferred

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
	naughty.notify({
		preset = naughty.config.presets.critical,
		title = "Oops, there were errors during startup!",
		text = awesome.startup_errors,
	})
end

local nconf = naughty.config
nconf.presets.critical.bg = "#ff5555"
nconf.presets.critical.fg = "#131313"
nconf.presets.critical.border_width = 0
nconf.presets.normal.border_width = 2
-- Handle runtime errors after startup
do
	local in_error = false
	awesome.connect_signal("debug::error", function(err)
		-- Make sure we don't go into an endless error loop
		if in_error then
			return
		end
		in_error = true

		naughty.notify({
			preset = naughty.config.presets.critical,
			title = "Oops, an error happened!",
			text = tostring(err),
		})
		in_error = false
	end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init('~/.config/awesome/themes/elegant_top_bar/theme.lua')

local fontconfig = io.open("/home/gurpinder/font.txt", "r")
if fontconfig then
    -- Read the content of the file
    local content = fontconfig:read("*a")
    fontconfig:close()
    if content and #content > 0 then
        beautiful.font = content
	end
end

local widget_mode = beautiful.widget_mode or "dark-mode"

local WIDGET_PATH = HOME_PATH.."/.config/awesome/icons/widgets/"..widget_mode

-- This is used later as the default terminal and editor to run.
terminal = "kitty"
editor = os.getenv("EDITOR") or "vi"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

show_titlebars = beautiful.titlebar or false

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = beautiful.de_mode and {
	awful.layout.suit.floating,
} or {
	awful.layout.suit.max,
	awful.layout.suit.tile,
}
-- awful.layout.suit.max,
-- awful.layout.suit.tile,
-- awful.layout.suit.floating,
-- awful.layout.suit.max.fullscreen,
-- awful.layout.suit.tile.left,
-- awful.layout.suit.tile.bottom,
-- awful.layout.suit.tile.top,
-- awful.layout.suit.fair,
-- awful.layout.suit.fair.horizontal,
-- awful.layout.suit.spiral,
-- awful.layout.suit.spiral.dwindle,
-- awful.layout.suit.magnifier,
-- awful.layout.suit.corner.nw,
-- awful.layout.suit.corner.ne,
-- awful.layout.suit.corner.sw,
-- awful.layout.suit.corner.se,

-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
	--    { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
	--    { "manual", terminal .. " -e man awesome" },
	{ "Edit Config", editor_cmd .. " " .. awesome.conffile },
	{ "Restart", awesome.restart },
	{
		"Quit",
		function()
			awesome.quit()
		end,
	},
}
screen_temp = {
	{
		"4500",
		function()
			awful.spawn("redshift -P -O 4500")
		end,
	},
	{
		"5500",
		function()
			awful.spawn("redshift -P -O 5500")
		end,
	},
	{
		"6500",
		function()
			awful.spawn("redshift -P -O 6500")
		end,
	},
}
powermenu = {
	{
		"Lock",
		function()
			awful.spawn("betterlockscreen -l")
		end,
	},
	{
		"LogOut",
		function()
			awesome.quit()
		end,
	},
	{
		"Shutdown",
		function()
			awful.spawn("shutdown now")
		end,
	},
	{
		"Reboot",
		function()
			awful.spawn("reboot")
		end,
	},
}
local themeswitcher = {
	{
		"Elegant - [T]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_top_bar")
		end,
	},
	{
		"Elegant - [B]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_bottom_bar")
		end,
	},
	{
		"Elegant - [D]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_dual_bar")
		end,
	},
	{
		"Elegant - [DE]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_de")
		end,
	},
	{
		"Elegant Purple- [T]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded/top_bar")
		end,
	},
	{
		"Elegant Purple- [B]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded/bottom_bar")
		end,
	},
	{
		"Elegant Purple- [D]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded/dual_bar")
		end,
	},
	{
		"Elegant Purple- [DE]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded/de")
		end,
	},
	{
		"Elegant Purple L- [T]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_light/top_bar")
		end,
	},
	{
		"Elegant Purple L- [B]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_light/bottom_bar")
		end,
	},
	{
		"Elegant Purple L- [D]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_light/dual_bar")
		end,
	},
	{
		"Elegant Purple L- [DE]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_light/de")
		end,
	},
	{
		"Elegant Sea- [T]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_sea/top_bar")
		end,
	},
	{
		"Elegant Sea- [B]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_sea/bottom_bar")
		end,
	},
	{
		"Elegant Sea- [D]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_sea/dual_bar")
		end,
	},
	{
		"Elegant Sea- [DE]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_sea/de")
		end,
	},
	{
		"Elegant Sea L- [T]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_sea_light/top_bar")
		end,
	},
	{
		"Elegant Sea L- [B]",
		function()
			awful.spawn(
				"bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_sea_light/bottom_bar"
			)
		end,
	},
	{
		"Elegant Sea L- [D]",
		function()
			awful.spawn(
				"bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_sea_light/dual_bar"
			)
		end,
	},
	{
		"Elegant Sea L- [DE]",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-theme.sh elegant_v2_rounded_sea_light/de")
		end,
	},
}

local modeswitcher = {
	{
		"Materia",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-mode materia")
		end,
	},
	{
		"Lavanda Light",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-mode lavanda_light")
		end,
	},
	{
		"Lavanda Dark",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-mode lavanda_dark")
		end,
	},
	{
		"Lavanda Sea Light",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-mode lavanda_sea_light")
		end,
	},
	{
		"Lavanda Sea Dark",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/switch-mode lavanda_sea_dark")
		end,
	},
}


local switch_font_family = {
	{
		"Monospace",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/scripts/font.sh 'monospace 11'")
		end,
	},
	{
		"Sans Serif",
		function()
			awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/scripts/font.sh 'sans-serif 12'")
		end,
	},
}

local function transform (str)
	return str
	-- return string.upper(str)
end

local mymainmenu = awful.menu({
	items = {
		{ transform("AwesomeWM"), myawesomemenu }, --beautiful.awesome_icon },
		{ transform("Power menu"), powermenu },
		{ transform("Screen temp"), screen_temp },
		{ transform("Switch theme"), themeswitcher },
		{ transform("Switch mode"), modeswitcher },
		{ transform("Switch font"), switch_font_family },
		{ transform("Terminal"), terminal },
	},
})

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar

show_layoutbox_icon = false

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
	awful.button({}, 1, function(t)
		t:view_only()
	end),
	awful.button({ modkey }, 1, function(t)
		if client.focus then
			client.focus:move_to_tag(t)
		end
	end),
	awful.button({}, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, function(t)
		if client.focus then
			client.focus:toggle_tag(t)
		end
	end),
	awful.button({}, 4, function(t)
		awful.tag.viewnext(t.screen)
	end),
	awful.button({}, 5, function(t)
		awful.tag.viewprev(t.screen)
	end)
)

local tasklist_buttons = gears.table.join(
	awful.button({}, 1, function(c)
		if c == client.focus then
			c.minimized = true
		else
			c:emit_signal("request::activate", "tasklist", { raise = true })
		end
	end),
	awful.button({}, 3, function()
		awful.menu.client_list({ theme = { width = 250 } })
	end),
	awful.button({}, 4, function()
		awful.client.focus.byidx(1)
	end),
	awful.button({}, 5, function()
		awful.client.focus.byidx(-1)
	end)
)

local function set_wallpaper(s)
	-- Wallpaper
	if beautiful.wallpaper then
		local wallpaper = beautiful.wallpaper
		-- If wallpaper is a function, call it with the screen
		if type(wallpaper) == "function" then
			wallpaper = wallpaper(s)
		end
		gears.wallpaper.maximized(wallpaper, s, true)
	end
end

local workflow_apps = function()
	awful.spawn.single_instance("firefox-developer-edition", {})
	awful.spawn.single_instance("code", {})
	awful.spawn.single_instance("insomnia", {})
	awful.spawn.single_instance("bitwarden-desktop", {})
	awful.spawn.single_instance("thunderbird", {})
	awful.spawn.single_instance("zoom", {})
	--awful.spawn.single_instance("Studio-3T",{})
end

local lock_system = function()
	-- awful.util.spawn("playerctl pause")
	awful.spawn("bash " .. HOME_PATH .. "/.config/awesome/scripts/powermenu")
	-- awful.util.spawn("betterlockscreen -l")
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

-- Each screen has its own tag table.
-- layout_names = {
-- 	"\u{e007}", -- web
-- 	"\u{f121}", -- code
-- 	"\u{f120}", -- term
-- 	"\u{f552}", -- test utils
-- 	"\u{f086}", -- comms
-- 	"\u{f53f}", -- media
-- 	"\u{f15c}", -- docs
-- 	"\u{e4e5}", -- virt
-- 	"\u{f023}", -- misc
-- }

-- layout_names = {
--     '1',
--     '2',
--     '3',
--     '4',
--     '5',
--     '6',
--     '7',
--     '8',
--     '9'
-- }

local layout_names = {
    '੧',
    '੨',
    '੩',
    '੪',
    '੫',
    '੬',
    '੭',
    '੮',
    '੯'
}

function toLower(list)
	for l = 1, #list do
		list[l] = string.lower(list[l])
	end
	return list
end

awful.screen.connect_for_each_screen(function(s)
	-- Wallpaper
	-- set_wallpaper(s)

	local layout = awful.layout.suit
	-- local layout_types = beautiful.de_mode
	-- 		and {
	-- 			layout.floating,
	-- 			layout.floating,
	-- 			layout.floating,
	-- 			layout.floating,
	-- 			layout.floating,
	-- 			layout.floating,
	-- 			layout.floating,
	-- 			layout.floating,
	-- 			layout.floating,
	-- 		}
	-- 	or {
	-- 		layout.max,
	-- 		layout.max,
	-- 		layout.tile,
	-- 		layout.max,
	-- 		layout.max,
	-- 		layout.max,
	-- 		layout.max,
	-- 		layout.max,
	-- 		layout.max,
	-- 	}
	-- awful.tag(layout_names, s, layout_types)

	-- TAGS
	awful.tag.add(layout_names[1],{
		-- icon = '',
		-- gap = 4,
		layout = beautiful.de_mode and layout.floating or layout.max,
		screen = s,
		selected = true
	})
	awful.tag.add(layout_names[2],{
		-- icon = '',
		-- gap = 4,
		layout = beautiful.de_mode and layout.floating or layout.max,
		screen = s,
	})
	awful.tag.add(layout_names[3],{
		-- icon = '',
		-- gap = 4,
		layout = beautiful.de_mode and layout.floating or layout.tile,
		screen = s,
	})
	awful.tag.add(layout_names[4],{
		-- icon = '',
		-- gap = 4,
		layout = beautiful.de_mode and layout.floating or layout.max,
		screen = s,
	})
	awful.tag.add(layout_names[5],{
		-- icon = '',
		-- gap = 4,
		layout = beautiful.de_mode and layout.floating or layout.max,
		screen = s,
	})
	awful.tag.add(layout_names[6],{
		-- icon = '',
		-- gap = 4,
		layout = beautiful.de_mode and layout.floating or layout.max,
		screen = s,
	})
	awful.tag.add(layout_names[7],{
		-- icon = '',
		-- gap = 4,
		layout = beautiful.de_mode and layout.floating or layout.max,
		screen = s,
	})
	awful.tag.add(layout_names[8],{
		-- icon = '',
		-- gap = 4,
		layout = beautiful.de_mode and layout.floating or layout.max,
		screen = s,
	})
	awful.tag.add(layout_names[9],{
		-- icon = '',
		-- gap = 4,
		layout = beautiful.de_mode and layout.floating or layout.max,
		screen = s,
	})
	--


	systray = wibox.widget.systray()
	systray.visible = true
	-- systray:set_base_size(48)

	-- Create a promptbox for each screen
	s.mypromptbox = awful.widget.prompt({
		prompt = " >> ",
		with_shell = false,
	})
	-- Create an imagebox widget which will contain an icon indicating which layout we're using.
	-- We need one layoutbox per screen.
	s.mylayoutbox = awful.widget.layoutbox(s)
	s.mylayoutbox:buttons(gears.table.join(
		awful.button({}, 1, function()
			awful.layout.inc(1)
		end),
		awful.button({}, 3, function()
			awful.layout.inc(-1)
		end),
		awful.button({}, 4, function()
			awful.layout.inc(1)
		end),
		awful.button({}, 5, function()
			awful.layout.inc(-1)
		end)
	))
	s.mylayoutbox.visible = false

	-- Create a taglist widget
	s.mytaglist = awful.widget.taglist({
		screen = s,
		filter = awful.widget.taglist.filter.all,
		buttons = taglist_buttons,
		-- style    = {
		-- 	shape_border_width = 1,
		-- 	shape_border_color = '#2E3137',
		-- 	shape  = beautiful.rounded_widget and gears.shape.rounded_rect or null,
		-- },
		layout = {
			spacing_widget = {
				{
					-- forced_width  = 20,
					-- forced_height = 32,
					thickness = 4,
					color = beautiful.primary_widget_bg_color or "transparent",
					widget = wibox.widget.separator,
				},
				-- valign = 'center',
				-- halign = 'center',
				widget = wibox.container.place,
			},
			spacing = 4,
			layout = wibox.layout.fixed.horizontal,
		},
	})

	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist({
		screen = s,
		filter = awful.widget.tasklist.filter.currenttags,
		buttons = tasklist_buttons,
		style = {
			shape_border_width = 1,
			shape_border_color = beautiful.tasklist_border_color or "#2E3137",
			shape = beautiful.rounded_widget and gears.shape.rounded_rect or null,
		},
		layout = {
			spacing_widget = {
				{
					forced_width = 20,
					forced_height = 24,
					thickness = 0,
					-- color         = '#94e2d5',
					widget = wibox.widget.separator,
				},
				valign = "center",
				halign = "center",
				widget = wibox.container.place,
			},
			spacing = 4,
			layout = wibox.layout.fixed.horizontal,
		},
		widget_template = {
			{
				{
					{
						{
							id = "clienticon",
							widget = awful.widget.clienticon,
						},
						right = 8,
						top = beautiful.de_mode and 2 or 0,
						bottom = beautiful.de_mode and 0 or 1,
						widget = wibox.container.margin,
					},
					{
						id = "text_role",
						widget = wibox.widget.textbox,
					},
					layout = wibox.layout.fixed.horizontal,
				},
				left = 8,
				top = beautiful.de_mode and 2 or 0,
				bottom = beautiful.de_mode and 0 or 1,
				widget = wibox.container.margin,
			},
			id = "background_role",
			forced_width = beautiful.variant == "dual-bar" and 200 or 100,
			forced_height = 32,
			widget = wibox.container.background,
			create_callback = function(self, c, index, objects) --luacheck: no unused
				self:get_children_by_id("clienticon")[1].client = c
			end,
		},
	})

	-- WIDGETS

	function widget_template(widget, bg, fg, darken, icon)
		darken = darken or false
		if darken then
			bg = bg .. "CC"
		end
		if icon then
			return wibox.widget({
				{
					{
						icon,
						left = 6,
						top = 6,
						bottom = 6,
						right = 6,
						widget = wibox.container.margin
					},
					widget,
					widget = wibox.layout.fixed.horizontal
				},
				shape = beautiful.rounded_widget and gears.shape.rounded_rect or null,
				bg = bg or "transparent",
				fg = fg or "#ffffff",
				widget = wibox.container.background,
			})
		else
			return wibox.widget({
				widget,
				shape = beautiful.rounded_widget and gears.shape.rounded_rect or null,
				bg = bg or "transparent",
				fg = fg or "#ffffff",
				widget = wibox.container.background,
			})
		end
	end

	mem_widget_info = awful.widget.watch(
		"bash " .. HOME_PATH .. "/.config/awesome/scripts/memory.sh",
		5,
		function(widget, stdout)
			widget:set_text("" .. stdout)
		end
	)
	cpu_widget_info = awful.widget.watch(
		"bash " .. HOME_PATH .. "/.config/awesome/scripts/cpu.sh",
		5,
		function(widget, stdout)
			widget:set_text("" .. stdout)
		end
	)

	cpu_widget_info.valign = "center"

	fs_widget_info = awful.widget.watch(
		"bash " .. HOME_PATH .. "/.config/awesome/scripts/fs.sh /",
		20,
		function(widget, stdout)
			widget:set_text("" .. stdout)
		end
	)
	fs_home_widget_info = awful.widget.watch(
		"bash " .. HOME_PATH .. "/.config/awesome/scripts/fs.sh /home",
		20,
		function(widget, stdout)
			widget:set_text(" \u{f0a0}  " .. stdout)
		end
	)
	battery_widget_info = awful.widget.watch(
		"bash " .. HOME_PATH .. "/.config/awesome/scripts/battery.sh",
		15,
		function(widget, stdout)
			widget:set_text("" .. stdout)
		end
	)

	function widget_label(label)
		return "  <b>" .. label .. "</b>  "
	end

	local cal_icon = wibox.widget.imagebox()
	cal_icon:set_image(WIDGET_PATH.."/clock.svg")
	clock_widget = wibox.widget({
		-- {
		-- 	{
		-- 		markup = "  \u{f017}  ",
		-- 		font = beautiful.icon_font,
		-- 		widget = wibox.widget.textbox,
		-- 	},
		-- 	bg = beautiful.clock_widget_bg_color .. "CC" or "transparent",
		-- 	fg = beautiful.clock_widget_fg_color or "#ffffff",
		-- 	widget = wibox.container.background,
		-- },
		{
			{	{
					cal_icon,
					left = 6,
					top = 6,
					bottom = 6,
					right = 6,
					widget = wibox.container.margin
				},
				{
					format = "%a %b %d, <b>%I:%M %p </b>",
					widget = wibox.widget.textclock,
				},
				widget = wibox.layout.fixed.horizontal,
			},
			shape = beautiful.rounded_widget and gears.shape.rounded_rect or null,
			bg = beautiful.clock_widget_bg_color or "transparent",
			fg = beautiful.clock_widget_fg_color or "#ffffff",
			widget = wibox.container.background,
		},
		widget = wibox.layout.fixed.horizontal,
	})

	calendar_location = beautiful.calendar_loc and beautiful.calendar_loc or "br"

	local month_calendar = awful.widget.calendar_popup.month({
		start_sunday = true,
		screen = s,
	})
	local year_calendar = awful.widget.calendar_popup.year({
		start_sunday = true,
		screen = s,
	})
	month_calendar:attach(clock_widget, calendar_location, {
		on_hover = true,
	})
	year_calendar:attach(clock_widget, calendar_location, {
		on_hover = false,
	})
	clock_widget:connect_signal("button::press", function(_, _, _, button, _)
		-- button 1 - left click, 2 - middle click , 3 - right click
		-- if button == 3 then
		--     year_calendar:toggle()
		-- end
	end)


	local bat_icon = wibox.widget.imagebox()
	bat_icon:set_image(WIDGET_PATH.."/battery-4-outline.svg")
	battery_widget = wibox.widget({
		widget_template(
			battery_widget_info, 
			beautiful.battery_widget_bg_color, 
			beautiful.battery_widget_fg_color,
			false, 
			bat_icon),
		widget = wibox.layout.fixed.horizontal,
	})
	battery_widget:connect_signal("button::press", function(_, _, _, button, _)
		if button == 1 then
			awful.spawn("lxqt-config-powermanagement")
		end
	end)

	local cpu_icon = wibox.widget.imagebox()
	cpu_icon:set_image(WIDGET_PATH.."/cpu.svg")
	cpu_widget = wibox.widget({
		widget_template(
			cpu_widget_info, 
			beautiful.cpu_widget_bg_color, 
			beautiful.cpu_widget_fg_color,
			false,
			cpu_icon),
		widget = wibox.layout.fixed.horizontal,
	})
	cpu_widget:connect_signal("button::press", function(_, _, _, button, _)
		if button == 1 then
			awful.spawn(terminal .. " -e btop")
		end
	end)

	local mem_icon = wibox.widget.imagebox()
	mem_icon:set_image(WIDGET_PATH.."/memory.svg")
	mem_widget = wibox.widget({
		widget_template(
			mem_widget_info, 
			beautiful.mem_widget_bg_color, 
			beautiful.mem_widget_fg_color,
			false,
			mem_icon),
		widget = wibox.layout.fixed.horizontal,
	})
	mem_widget:connect_signal("button::press", function(_, _, _, button, _)
		if button == 1 then
			awful.spawn(terminal .. " -e btop")
		end
	end)

	local hdd_icon = wibox.widget.imagebox()
	hdd_icon:set_image(WIDGET_PATH.."/hard-disk-line.svg")
	fs_widget = wibox.widget({
		widget_template(
			fs_widget_info, 
			beautiful.fs_widget_bg_color, 
			beautiful.fs_widget_fg_color, 
			false, 
			hdd_icon),
		widget = wibox.layout.fixed.horizontal,
	})
	fs_widget:connect_signal("button::press", function(_, _, _, button, _)
		if button == 1 then
			awful.spawn(terminal .. " -e ncdu")
		end
	end)

	fs_home_widget = wibox.widget({
		widget_template({
			markup = widget_label("\u{f0a0}"),
			font = beautiful.icon_font,
			widget = wibox.widget.textbox,
		}, beautiful.primary_widget_bg_color, beautiful.fs2_widget_fg_color),
		widget_template(fs_home_widget_info, beautiful.primary_widget_bg_color, beautiful.fs2_widget_fg_color),
		widget = wibox.layout.fixed.horizontal,
	})
	fs_home_widget:connect_signal("button::press", function(_, _, _, button, _)
		if button == 1 then
			awful.spawn(terminal .. " -e ncdu")
		end
	end)

	local user_icon = wibox.widget.imagebox()
	user_icon:set_image(WIDGET_PATH.."/happy-face.png")
	username_widget = wibox.widget({
		widget_template({
			-- markup = "  \u{f599} " .. USERNAME .. "  ",
			markup = "",
			font = beautiful.font,
			widget = wibox.widget.textbox,
		}, 
		beautiful.username_widget_bg_color, 
		beautiful.username_widget_fg_color, false, user_icon),
		widget = wibox.layout.fixed.horizontal,
	})

	function launch_workspace_apps()
		local apps = {
			"firefox",
			"code",
			"insomnia",
			"mailspring",
			"mattermost-desktop",
			"bitwarden-desktop",
		}
		for apps_count = 1, #apps do
			awful.spawn.raise_or_spawn(apps[apps_count])
		end
	end

	username_widget:connect_signal("button::press", function(_, _, _, button, _)
		if button == 1 then
			systray.visible = not systray.visible
			if systray.visible then
				s.mylayoutbox.visible = true
			else
				s.mylayoutbox.visible = false
			end
		end
		if button == 2 then
			launch_workspace_apps()
		end
		if button == 3 then
			systray.visible = not systray.visible
		end
	end)

	if beautiful.variant == "single-bar" then
		-- BOTTOM BAR
		s.bottom_bar = awful.wibar({
			position = beautiful.bar_location and beautiful.bar_location or "bottom",
			ontop = false,
			screen = s,
			height = beautiful.de_mode and 38 or 36,
		})
		s.bottom_bar:setup({
			expand = "center",
			layout = wibox.layout.align.horizontal,
			{
				{

					{
						{
							s.mytaglist,
							left = 8,
							right = 8,
							widget = wibox.container.margin,
						},
						bg = beautiful.taglist_bg or "transparent",
						shape = beautiful.rounded_widget and gears.shape.rounded_rect or null,
						widget = wibox.container.background,
					},
					s.mypromptbox,
					spacing = 4,
					layout = wibox.layout.fixed.horizontal,
				},
				margins = 2,
				widget = wibox.container.margin,
			},
			{
				s.mytasklist,
				top = 2,
				left = 2,
				bottom = 2,
				widget = wibox.container.margin,
			},
			{ -- Right widgets
				{
					
					cpu_widget,
					mem_widget,
					fs_widget,
					-- fs_home_widget,
					battery_widget,
					clock_widget,
					username_widget,
					{
						s.mylayoutbox,
						top = beautiful.de_mode and 4 or 4,
						bottom = beautiful.de_mode and 4 or 4,
						widget = wibox.container.margin,
					},
					{
						systray,
						top = beautiful.de_mode and 4 or 2,
						bottom = beautiful.de_mode and 4 or 2,
						widget = wibox.container.margin,
					},
					spacing = 3,
					layout = wibox.layout.fixed.horizontal,
				},
				top = beautiful.de_mode and 2 or 2,
				bottom = beautiful.de_mode and 2 or 2,
				widget = wibox.container.margin,
			},
		})
	end
	
	if beautiful.variant == "dual-bar" then
		s.top_bar = awful.wibar({ position = "top", screen = s, height = 36 })
		s.bottom_bar = awful.wibar({ position = "bottom", screen = s, height = 36 })
		s.top_bar:setup({
			expand = "none",
			layout = wibox.layout.align.horizontal,
			{ -- Right widgets
				{
					cpu_widget,
					mem_widget,
					fs_widget,
					-- fs_home_widget,
					spacing = 2,
					layout = wibox.layout.fixed.horizontal,
				},
				--    top = 2,
				--    bottom = 2,
				margins = 2,
				widget = wibox.container.margin,
			},
			{
				{
					{
						{
							s.mytaglist,
							left = 8,
							right = 8,
							widget = wibox.container.margin,
						},
						bg = beautiful.primary_widget_bg_color or "transparent",
						shape = beautiful.rounded_widget and gears.shape.rounded_rect or null,
						widget = wibox.container.background,
					},
					s.mypromptbox,
					spacing = 4,
					layout = wibox.layout.fixed.horizontal,
				},
				margins = 4,
				widget = wibox.container.margin,
			},
			{
				clock_widget,
				margins = 2,
				widget = wibox.container.margin,
			},
		})

		s.bottom_bar:setup({
			expand = "center",
			layout = wibox.layout.align.horizontal,
			{
				{
					s.mylayoutbox,
					spacing = 2,
					layout = wibox.layout.fixed.horizontal,
				},
				margins = 2,
				widget = wibox.container.margin,
			},
			{
				s.mytasklist,
				top = 2,
				bottom = 2,
				shape = beautiful.rounded_widget and gears.shape.rounded_rect or null,
				widget = wibox.container.margin,
			},
			{ -- Right widgets
				{
					username_widget,
					{
						systray,
						margins = 2,
						widget = wibox.container.margin,
					},
					spacing = 2,
					layout = wibox.layout.fixed.horizontal,
				},
				top = 2,
				bottom = 2,
				widget = wibox.container.margin,
			},
		})
	end
end)
-- }}}

-- {{{ Mouse bindings
-- root.buttons(gears.table.join(
--     awful.button({ }, 3, function () mymainmenu:toggle() end),
--     awful.button({ }, 4, awful.tag.viewnext),
--     awful.button({ }, 5, awful.tag.viewprev)
-- ))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(

	--    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
	--              {description="show help", group="awesome"}),
	awful.key({ modkey }, "Left", awful.tag.viewprev, { description = "view previous", group = "tag" }),
	awful.key({ modkey }, "Right", awful.tag.viewnext, { description = "view next", group = "tag" }),
	awful.key({ modkey }, "Escape", awful.tag.history.restore, { description = "go back", group = "tag" }),

	awful.key({ modkey }, "j", function()
		awful.client.focus.byidx(1)
	end, { description = "focus next by index", group = "client" }),
	awful.key({ modkey }, "k", function()
		awful.client.focus.byidx(-1)
	end, { description = "focus previous by index", group = "client" }),
	awful.key({ modkey }, "w", function()
		mymainmenu:show()
	end, { description = "show main menu", group = "awesome" }),

	-- Layout manipulation
	awful.key({ modkey, "Shift" }, "j", function()
		awful.client.swap.byidx(1)
	end, { description = "swap with next client by index", group = "client" }),
	awful.key({ modkey, "Shift" }, "k", function()
		awful.client.swap.byidx(-1)
	end, { description = "swap with previous client by index", group = "client" }),
	awful.key({ modkey, "Control" }, "j", function()
		awful.screen.focus_relative(1)
	end, { description = "focus the next screen", group = "screen" }),
	awful.key({ modkey, "Control" }, "k", function()
		awful.screen.focus_relative(-1)
	end, { description = "focus the previous screen", group = "screen" }),
	awful.key({ modkey }, "u", awful.client.urgent.jumpto, { description = "jump to urgent client", group = "client" }),
	--[[ awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),
    ]]
	--

	-- Standard program
	awful.key({ modkey }, "Return", function()
		awful.spawn(terminal)
	end, { description = "open a terminal", group = "launcher" }),
	-- awful.key({ modkey, "Control" }, "r", awesome.restart,
	--           {description = "reload awesome", group = "awesome"}),
	--awful.key({ modkey, "Shift"   }, "q", awesome.quit,
	--{description = "quit awesome", group = "awesome"}),

	awful.key({ modkey }, "l", function()
		awful.tag.incmwfact(0.05)
	end, { description = "increase master width factor", group = "layout" }),
	awful.key({ modkey }, "h", function()
		awful.tag.incmwfact(-0.05)
	end, { description = "decrease master width factor", group = "layout" }),
	awful.key({ modkey, "Shift" }, "h", function()
		awful.tag.incnmaster(1, nil, true)
	end, { description = "increase the number of master clients", group = "layout" }),
	awful.key({ modkey, "Shift" }, "l", function()
		awful.tag.incnmaster(-1, nil, true)
	end, { description = "decrease the number of master clients", group = "layout" }),
	awful.key({ modkey, "Control" }, "h", function()
		awful.tag.incncol(1, nil, true)
	end, { description = "increase the number of columns", group = "layout" }),
	awful.key({ modkey, "Control" }, "l", function()
		awful.tag.incncol(-1, nil, true)
	end, { description = "decrease the number of columns", group = "layout" }),
	awful.key({ modkey }, "space", function()
		awful.layout.inc(1)
	end, { description = "select next", group = "layout" }),
	awful.key({ modkey, "Shift" }, "space", function()
		awful.layout.inc(-1)
	end, { description = "select previous", group = "layout" }),

	awful.key({ modkey }, "Up", function()
		local c = awful.client.restore()
		-- Focus restored client
		if c then
			c:emit_signal("request::activate", "key.unminimize", { raise = true })
		end
	end, { description = "restore minimized", group = "client" }),

	-- Prompt
	awful.key({ modkey }, "r", function()
		awful.screen.focused().mypromptbox:run()
	end, { description = "run prompt", group = "launcher" }),

	-- awful.key({ modkey }, "x",
	--           function ()
	--               awful.prompt.run {
	--                 prompt       = "Run Lua code: ",
	--                 textbox      = awful.screen.focused().mypromptbox.widget,
	--                 exe_callback = awful.util.eval,
	--                 history_path = awful.util.get_cache_dir() .. "/history_eval"
	--               }
	--           end,
	--           {description = "lua execute prompt", group = "awesome"}),

	-- Menubar
	awful.key({ modkey }, "p", function()
		menubar.show()
	end, { description = "show the menubar", group = "launcher" }),

	-- media keys
	awful.key({}, "XF86AudioRaiseVolume", function()
		awful.util.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5000")
	end),
	awful.key({}, "XF86AudioLowerVolume", function()
		awful.util.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5000")
	end),
	awful.key({}, "XF86AudioMute", function()
		awful.util.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")
	end),
	awful.key({}, "XF86AudioPlay", function()
		awful.util.spawn("playerctl play-pause")
	end),
	awful.key({}, "XF86AudioPrev", function()
		awful.util.spawn("playerctl previous")
	end),
	awful.key({}, "XF86AudioNext", function()
		awful.util.spawn("playerctl next")
	end),

	-- backlight control
	awful.key({}, "XF86MonBrightnessUp", function()
		awful.util.spawn("xbacklight -inc 15")
	end),
	awful.key({}, "XF86MonBrightnessDown", function()
		awful.util.spawn("xbacklight -dec 15")
	end),

	-- lock screen
	awful.key({ modkey, "Mod1" }, "Delete", lock_system),

	-- workspace apps
	awful.key({ modkey, "Shift", "Control" }, "Return", workflow_apps),

	-- programs
	awful.key({ modkey }, "b", function(c)
		awful.util.spawn("firefox")
	end),
	awful.key({ modkey }, "e", function(c)
		awful.util.spawn("pcmanfm-qt")
	end),
	awful.key({ modkey, "Shift" }, "e", function(c)
		awful.util.spawn(terminal .. " -e ranger")
	end),
	awful.key({ modkey, "Shift" }, "r", function(c)
		awful.util.spawn("obs --startrecording")
	end),
	awful.key({ modkey }, "semicolon", function(c)
		awful.util.spawn("code")
	end),
	awful.key({ modkey }, "d", function(c)
		awful.util.spawn("rofi -show drun")
	end),
	awful.key({ modkey }, "x", function(c)
		awful.util.spawn("rofi -show emoji")
	end),
	awful.key({ modkey }, "s", function(c)
		awful.util.spawn("rofi -show ssh")
	end),
	awful.key({ modkey }, "c", function(c)
		awful.util.spawn("rofi -show calc -hint-welcome '' -hint-result ''")
	end),
	awful.key({ modkey }, "Tab", function(c)
		awful.util.spawn("rofi -show window")
	end),

	awful.key({ modkey }, "Print", function(c)
		awful.util.spawn("flameshot gui")
	end),
	awful.key({ modkey, "Shift" }, "Print", function(c)
		awful.util.spawn("flameshot full")
	end)
)

clientkeys = gears.table.join(
	awful.key({ modkey, "Shift" }, "s", function(c)
		c.sticky = not c.sticky
	end, { description = "sticky", group = "client" }),
	awful.key({ modkey }, "f", function(c)
		c.fullscreen = not c.fullscreen
		c:raise()
	end, { description = "toggle fullscreen", group = "client" }),
	awful.key({ modkey, "Shift" }, "q", function(c)
		c:kill()
	end, { description = "close", group = "client" }),
	awful.key(
		{ modkey, "Shift" },
		"f",
		awful.client.floating.toggle,
		{ description = "toggle floating", group = "client" }
	),
	awful.key({ modkey, "Control" }, "Return", function(c)
		c:swap(awful.client.getmaster())
	end, { description = "move to master", group = "client" }),
	awful.key({ modkey }, "o", function(c)
		c:move_to_screen()
	end, { description = "move to screen", group = "client" }),
	awful.key({ modkey }, "t", function(c)
		c.ontop = not c.ontop
	end, { description = "toggle keep on top", group = "client" }),
	awful.key({ modkey }, "Down", function(c)
		-- The client currently has the input focus, so it cannot be
		-- minimized, since minimized clients can't have the focus.
		c.minimized = true
	end, { description = "minimize", group = "client" }),
	awful.key({ modkey }, "m", function(c)
		c.maximized = not c.maximized
		c:raise()
	end, { description = "(un)maximize", group = "client" }),
	awful.key({ modkey, "Control" }, "m", function(c)
		c.maximized_vertical = not c.maximized_vertical
		c:raise()
	end, { description = "(un)maximize vertically", group = "client" }),
	awful.key({ modkey, "Shift" }, "m", function(c)
		c.maximized_horizontal = not c.maximized_horizontal
		c:raise()
	end, { description = "(un)maximize horizontally", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
	globalkeys = gears.table.join(
		globalkeys,
		-- View tag only.
		awful.key({ modkey }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				tag:view_only()
			end
		end, { description = "view tag #" .. i, group = "tag" }),
		-- Toggle tag display.
		awful.key({ modkey, "Control" }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				awful.tag.viewtoggle(tag)
			end
		end, { description = "toggle tag #" .. i, group = "tag" }),
		-- Move client to tag.
		awful.key({ modkey, "Shift" }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:move_to_tag(tag)
				end
			end
		end, { description = "move focused client to tag #" .. i, group = "tag" }),
		-- Toggle tag on focused client.
		awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:toggle_tag(tag)
				end
			end
		end, { description = "toggle focused client on tag #" .. i, group = "tag" })
	)
end

clientbuttons = gears.table.join(
	awful.button({}, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
	end),
	awful.button({ modkey }, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.move(c)
	end),
	awful.button({ modkey }, 3, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.resize(c)
	end)
)
-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
	-- All clients will match this rule.
	{
		rule = {},
		properties = {
			border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = clientkeys,
			buttons = clientbuttons,
			screen = awful.screen.preferred,
			placement = awful.placement.no_overlap + awful.placement.no_offscreen,
			size_hints_honor = false,
		},
	},

	-- Floating clients.
	{
		rule_any = {
			instance = {
				"DTA", -- Firefox addon DownThemAll.
				"copyq", -- Includes session name in class.
				"pinentry",
			},
			class = {
				"Arandr",
				"Blueman-manager",
				"Galculator",
				"Gpick",
				"Kruler",
				"MessageWin", -- kalarm.
				"Sxiv",
				"Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
				"Wpa_gui",
				"veromix",
				"Pavucontrol",
				"xtightvncviewer",
				"Nitrogen"
			},

			-- Note that the name property shown in xprop might be set slightly after creation of the client
			-- and the name shown there might not match defined rules here.
			name = {
				"Event Tester", -- xev.
			},
			role = {
				"AlarmWindow", -- Thunderbird's calendar.
				"ConfigManager", -- Thunderbird's about:config.
				"pop-up", -- e.g. Google Chrome's (detached) Developer Tools.
			},
		},
		properties = { floating = true },
	},

	-- Add titlebars to normal clients and dialogs
	{
		rule_any = { type = { "normal" } },
		properties = {
			titlebars_enabled = show_titlebars,
			border_width = beautiful.enable_border and beautiful.border_width or 0,
		},
	},
	{
		rule_any = { type = { "dialog" } },
		properties = {
			titlebars_enabled = show_titlebars,
			border_width = 1,
			placement = awful.placement.centered,
		},
	},

	-- Spawn floating clients in center
	-- { rule_any = { type = { "dialog" } }, properties = {  } },

	-- Set Firefox to always map on the tag named "2" on screen 1.
	-- { rule = { class = "Firefox" },
	--   properties = { screen = 1, tag = "2" } },
	-- App workspace rules
	{ rule = { class = "Brave-browser" }, properties = { screen = active_screen, tag = layout_names[1] } },
	{ rule = { class = "firefox" }, properties = { screen = active_screen, tag = layout_names[1] } },
	{ rule = { class = "firefox-developer-edition" }, properties = { screen = active_screen, tag = layout_names[1] } },
	{ rule = { class = "Code" }, properties = { screen = active_screen, tag = layout_names[2] } },
	{ rule = { class = "code-oss" }, properties = { screen = active_screen, tag = layout_names[2] } },
	{ rule = { class = "jetbrains-studio" }, properties = { screen = active_screen, tag = layout_names[2] } },
	{ rule = { class = "MongoDB Compass" }, properties = { screen = active_screen, tag = layout_names[4] } },
	{ rule = { class = "Studio 3T" }, properties = { screen = active_screen, tag = layout_names[4] } },
	{ rule = { class = "robo3t" }, properties = { screen = active_screen, tag = layout_names[4] } },
	{ rule = { class = "Insomnia" }, properties = { screen = active_screen, tag = layout_names[4] } },
	{ rule = { class = "Bitwarden" }, properties = { screen = active_screen, tag = layout_names[9] } },
	{ rule = { class = "Filezilla" }, properties = { screen = active_screen, tag = layout_names[4] } },
	{ rule = { class = "thunderbird" }, properties = { screen = active_screen, tag = layout_names[5] } },
	{ rule = { class = "Mailspring" }, properties = { screen = active_screen, tag = layout_names[5] } },
	{ rule = { class = "TelegramDesktop" }, properties = { screen = active_screen, tag = layout_names[5] } },
	{ rule = { class = "discord" }, properties = { screen = active_screen, tag = layout_names[5] } },
	{ rule = { class = "Mattermost" }, properties = { screen = active_screen, tag = layout_names[5] } },
	{ rule = { class = "zoom" }, properties = { screen = active_screen, tag = layout_names[5] } },
	{ rule = { class = "obs" }, properties = { screen = active_screen, tag = layout_names[6] } },
	{ rule = { class = "krita" }, properties = { screen = active_screen, tag = layout_names[6] } },
	{ rule = { class = "Gimp-2.10" }, properties = { screen = active_screen, tag = layout_names[6] } },
	{ rule = { class = "Shotcut" }, properties = { screen =active_screen, tag = layout_names[6] } },
	{ rule = { class = "Spotify" }, properties = { screen = active_screen, tag = layout_names[6] } },
	{ rule = { class = "Inkscape" }, properties = { screen = active_screen, tag = layout_names[6] } },
	{ rule = { class = "resolve" }, properties = { screen = active_screen, tag = layout_names[6] } },
	{ rule = { class = "Ghb" }, properties = { screen = active_screen, tag = layout_names[6] } },
	{ rule = { class = "Xournalpp" }, properties = { screen = active_screen, tag = layout_names[7] } },
	{ rule = { class = "Focalboard-app" }, properties = { screen = active_screen, tag = layout_names[7] } },
	{ rule = { class = "obsidian" }, properties = { screen = active_screen, tag = layout_names[7] } },
	{ rule = { class = "libreoffice-writer" }, properties = { screen = active_screen, tag = layout_names[7] } },
	{ rule = { class = "Virt-manager" }, properties = { screen = active_screen, tag = layout_names[8] } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
	-- Set the windows at the slave,
	-- i.e. put it at the end of others instead of setting it master.
	-- if not awesome.startup then awful.client.setslave(c) end

	if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
		-- Prevent clients from being unreachable after screen count changes.
		awful.placement.no_offscreen(c)
	end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
	-- buttons for the titlebar
	local buttons = gears.table.join(
		awful.button({}, 1, function()
			c:emit_signal("request::activate", "titlebar", { raise = true })
			awful.mouse.client.move(c)
		end),
		awful.button({}, 3, function()
			c:emit_signal("request::activate", "titlebar", { raise = true })
			awful.mouse.client.resize(c)
		end)
	)

	local top_titlebar = awful.titlebar(c, {
		size = beautiful.de_mode and 32 or 0,
		position = "top",
	})

	top_titlebar:setup({
		{ -- Left
			-- awful.titlebar.widget.iconwidget(c),
			-- buttons = buttons,
			{
				awful.titlebar.widget.stickybutton(c),
				awful.titlebar.widget.ontopbutton(c),
				spacing = 8,
				layout = wibox.layout.fixed.horizontal,
			},
			margins = 6,
			widget = wibox.container.margin,
		},
		{ -- Middle
			{ -- Title
				align = "center",
				widget = awful.titlebar.widget.titlewidget(c),
			},
			buttons = buttons,
			layout = wibox.layout.flex.horizontal,
		},
		{ -- Right
			{
				awful.titlebar.widget.floatingbutton(c),
				margins = 8,
				widget = wibox.container.margin,
			},
			{
				{
					awful.titlebar.widget.minimizebutton(c),
					awful.titlebar.widget.maximizedbutton(c),
					awful.titlebar.widget.closebutton(c),
					spacing = 16,
					layout = wibox.layout.fixed.horizontal(),
				},
				margins = 10,
				widget = wibox.container.margin,
			},
			layout = wibox.layout.align.horizontal,
		},
		layout = wibox.layout.align.horizontal,
	})
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
	c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

client.connect_signal("focus", function(c)
	c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
	c.border_color = beautiful.border_normal
end)

client.connect_signal("property::floating", function(c)
	if c.floating then
		c.border_width = 1
	else
		c.border_width = 1
	end
end)

-- }}}

-- client.connect_signal("property::class", function(c)
--     if c then
--         if c.class == "Spotify" then
--             local t = awful.tag.find_by_name(awful.screen.focused(), "6");
--             c:move_to_tag(t)
--         end

--         if string.find(c.class, "libreoffice")  then
--             local t = awful.tag.find_by_name(awful.screen.focused(), "7");
--             c:move_to_tag(t)
--         end
--     end
-- end)
-- Utility function to check if a value exists in an array
function containsValue(array, value)
    for _, v in ipairs(array) do
        if v == value then
            return true
        end
    end
    return false
end

local app_opacity = beautiful.app_opacity and beautiful.app_opacity or 0.8

local blur_excluded_apps = {"zoom", "Virt-manager"}

local function on_layout_change(c)
    local active_tag = awful.screen.focused().selected_tag
	local focused_class = c.class
    if active_tag.layout.name == "max" then
        -- Iterate through clients on the active tag
		local floating_window_exists = false
        for _, c in ipairs(active_tag:clients()) do
			local opacity = app_opacity
			-- awful.spawn("notify-send 'info' " ..tostring(c.content))
			if containsValue(blur_excluded_apps, c.class) then
				opacity = 1
			end
			-- Check if the client is focused and not a dialog
			if c.floating then
				floating_window_exists = true
			end
			if c == client.focus then
				-- If focused show the client in the tasklist
				c.skip_taskbar = false
				c.opacity = opacity -- Set opacity to 1 to make the window visible
			else
				if c.class ~= focused_class then
					c.skip_taskbar = false
					c.opacity = 0 -- Set opacity to 0 to make the window transparent (invisible)
				end
			end
        end
		if floating_window_exists then
			for _, c in ipairs(active_tag:clients()) do
				c.skip_taskbar = false
				c.opacity = 0.8
			end		
		end
    else
        for _, c in ipairs(active_tag:clients()) do
            c.skip_taskbar = false
            c.opacity = app_opacity
        end	
    end
end


client.connect_signal("focus", function(c) 
	on_layout_change(c)
end)

-- -- Define a function to handle layout changes
-- Connect the function to the layout change signal of all tags on all screens
for s in screen do
    for _, tag in pairs(s.tags) do
        tag:connect_signal("property::layout", function(c)
            on_layout_change(c)
        end)
    end
end

local hide_systray = gears.timer({
	timeout = 1,
	call_now = false,
	single_shot = true,
	autostart = true,
	callback = function(s)
		systray.visible = false
	end,
})


local autohide_systray = gears.timer({
	timeout = 15,
	call_now = false,
	autostart = true,
	single_shot = false,
	callback = function(s)
		systray.visible = false
		awful.screen.connect_for_each_screen(function(s)
			s.mylayoutbox.visible = false
		end)
	end,
})

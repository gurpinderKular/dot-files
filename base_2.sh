#!/bin/bash

sudo pacman -Syyu --noconfirm

echo "Installing packages"
sudo pacman -S stow wget curl htop lxappearance qt5ct kvantum nitrogen lxrandr bluez bluez-utils blueberry feh alacritty playerctl pavucontrol ranger rofi rofi-calc rofi-emoji imagemagick libsecret polkit lxqt-policykit networkmanager-openvpn openvpn libimobiledevice udisks2 ntfs-3g yt-dlp seahorse vim pcmanfm-qt zathura mpv flameshot firefox gnome-keyring gvfs zsh --noconfirm

stow */ --adopt

# install aur helper
yaypath="/home/$USER/yay"
git clone https://aur.archlinux.org/yay.git $yaypath
cd $yaypath
makepkg -si

# enable services
sudo systemctl enable bluetooth

# fix bluetooth icon
gsettings set org.blueberry use-symbolic-icons false

# make zsh default shell
chsh -s /usr/bin/zsh


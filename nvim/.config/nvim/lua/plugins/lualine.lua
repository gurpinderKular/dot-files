return {
  {
    "nvim-lualine/lualine.nvim",
    event = "VeryLazy",
    opts = function()
      local icons = require("lazyvim.config").icons
      local Util = require("lazyvim.util")
      return {
        options = {
          section_separators = { left = '', right = '' },
          component_separators = { left = '', right = '' }
        }
      }
    end
  }
}

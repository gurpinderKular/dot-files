
#! /bin/bash

#read -p "theme name:" THEME

THEME=$1

# awesomewm
AWESOME_THEME_PATH="~/.config/awesome/themes/$THEME/theme.lua"
sed -i "/awesome\/themes/c\beautiful.init('$AWESOME_THEME_PATH')" /home/$USER/.config/awesome/rc.lua

# restart awesome
echo 'awesome.restart()' | awesome-client

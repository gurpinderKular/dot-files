-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
--

local opt = vim.opt
opt.colorcolumn = "80"
opt.shiftwidth = 2
opt.relativenumber = true
opt.wrap = true

opt.list = true
opt.listchars:append "space:⋅"
opt.listchars:append "eol:↴"

opt.pumblend = 0

local float = { focusable = true, style = "minimal", border = "rounded", }

vim.lsp.handlers["textDocument/hover"] =
  vim.lsp.with(
  vim.lsp.handlers.hover,
  float
 )

vim.lsp.handlers["textDocument/signatureHelp"] =
  vim.lsp.with(
  vim.lsp.handlers.signature_help,
  float
)

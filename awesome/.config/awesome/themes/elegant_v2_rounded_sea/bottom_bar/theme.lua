local elegant_base = require "themes.elegant_v2_rounded_sea.base.theme"
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local theme = elegant_base

theme.titlebar      =   true
theme.calendar_loc  =   "br"
theme.variant       =   "single-bar"
theme.rounded_widget = true

theme.useless_gap           =   dpi(4)
theme.border_width          =   dpi(0)

return theme;
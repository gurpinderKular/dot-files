# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh_history

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files

# alias
alias yaylist="pacman -Qqm"
alias pacmanlist="pacman -Qqe"
alias cat="bat"
alias ls="lsd"
alias fetch="nitch"
alias e="nvim"
alias nv="nvim"
alias z="zellij"
alias ytd="yt-dlp "
alias ytdv="yt-dlp -f 22 "
alias ytdvfhd="yt-dlp -f 'bv*[height=1080]+ba' --merge-output-format mp4 "
alias ytda="yt-dlp -f 'ba' -x --audio-format mp3 "
alias ytdp="yt-dlp -f 22 --yes-playlist "
alias ytdpi="yt-dlp -f 22 --yes-playlist --output '%(playlist_index)s. %(title)s.%(ext)s'"

# zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
source ~/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/gurpinder/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/gurpinder/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/gurpinder/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/gurpinder/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


# pnpm
export PNPM_HOME="/home/gurpinder/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end
#

export PATH=/home/gurpinder/.local/bin:$PATH

# bun completions
[ -s "/home/gurpinder/.bun/_bun" ] && source "/home/gurpinder/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"

#!/bin/bash
yaru_path=/home/$USER/Yaru-Colors
git clone https://github.com/Jannomag/Yaru-Colors.git $yaru_path
cd $yaru_path
cp -a Icons/. /usr/share/icons/
cp -a Themes/. /usr/share/themes/
cp -a Wallpapers /home/gurpinder/

surfn_path=/home/$USER/Surfn
git clone https://github.com/erikdubois/Surfn.git $surfn_path
cd $surfn_path
cp -a surfn-icons/. /usr/share/icons/

kv_yaru_path=/home/$USER/KvYaru-Colors
git clone https://github.com/GabePoel/KvYaru-Colors.git $kv_yaru_path
cd $kv_yaru_path 
echo "y" | sh install.sh

sed -i '$ a QT_QPA_PLATFORMTHEME=qt5ct' /etc/environment

rm -rf $yaru_path $surfn_path $kv_yaru_path



#!/bin/bash

sudo pacman -Syyu --noconfirm

echo "Installing packages"
sudo pacman -S linux-headers base-devel xorg-server xorg-xinit xorg-xbacklight xorg-xinput xterm xdg-user-dirs sof-firmware awesome lightdm lightdm-gtk-greeter network-manager-applet --noconfirm

# enable services
sudo systemctl enable lightdm
sudo systemctl set-default graphical.target


